<?xml version="1.0" encoding="UTF-8"?>
<!--

-->
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="AddonButtonTogglers" version="1.0" date="2017 july" >
		<VersionSettings gameVersion="1.4.8" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<Author name="anon" email="" />
		<Description text="Adds more togglers and config pages to AddonButton" />
		<Dependencies>
			<Dependency name="LibAddonButton" />
		</Dependencies>
		<Files>
			<File name="AddonButtonTogglers.lua" />
		</Files>
		<!-- <SavedVariables>
			<SavedVariable name="AddonButtonTogglers.settings" />
		</SavedVariables> -->
		<OnInitialize>
			<CallFunction name="AddonButtonTogglers.OnInitialize" />
		</OnInitialize>
		<OnShutdown>
			<CallFunction name="AddonButtonTogglers.OnShutdown" />
    	</OnShutdown>
	</UiMod>
</ModuleFile>
