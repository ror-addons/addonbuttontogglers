AddonButtonTogglers = {}


function AddonButtonTogglers.OnInitialize()
	local buttons = AddonButtonTogglers.getAvailableAddons()

	if LibAddonButton then
		AddonButtonTogglers.registerButtons(buttons)
	end
	
	-- LibSlash.RegisterSlashCmd("AddonButtonTogglers", function(input) AddonButtonTogglers.HandleSlashCmd(input) end)
	TextLogAddEntry("Chat", 0, L"AddonButtonTogglers initialized")
end


function AddonButtonTogglers.getAvailableAddons() 
	local a = {}

	-------------------------------
	--- simple items
	-------------------------------
	if Motion then a[#a+1] = 		{registered = false, name =L"Motion <icon00258>", 		onLButton = Motion.Toggle} end
	--if MiracleGrow2 then a[#a+1] = 	{registered = false, name =L"MGRemix <icon20470>", 		onLButton = MiracleGrow2.toggle} end --, 	onRButton = function () IraConfig.Open(MiracleGrow2.nConfigTab) end
	if TomeTracker then a[#a+1] = 	{registered = false, name =L"TomeTracker <icon00059>", 	onLButton = TomeTracker.Journal.Toggle} end
	if RVMOD_Manager then a[#a+1] = {registered = false, name =L"RVMOD_Manager", 			onLButton = RVMOD_Manager.onLButtonUpToggleButton} end
	if Dascore then a[#a+1] = 		{registered = false, name =L"DaScore", 					onLButton = Dascore.Win1.Show} end
	if WSCT then a[#a+1] = 			{registered = false, name =L"WSCT", 					onLButton = WSCT.OpenMenu} end
	if ClosetGoblin then a[#a+1] = 	{registered = false, name =L"ClosetGoblin", 			onLButton = ClosetGoblinCharacterWindow.ShowOrHide} end
	if Pure then a[#a+1] = 			{registered = false, name =L"Pure", 					onLButton = Pure.ToggleConfig} end
	if Shinies then a[#a+1] = 		{registered = false, name =L"Shinies", 					onLButton = Shinies.Toggle} end
	if TomeTitan then a[#a+1] = 	{registered = false, name =L"TomeTitan", 				onLButton = TTitan.UI.ToggleShowing} end
	if LPET then a[#a+1] = 			{registered = false, name =L"Loyal Pet", 				onLButton = LPET.OpenMenu} end
	if ActionBarHide then a[#a+1] = {registered = false, name =L"ActionBarHide", 			onLButton = ActionBarHide.OptionsWindow} end
	if CCTV then a[#a+1] = 			{registered = false, name =L"CCTV", 					onLButton = CCTV.Slash} end
	if WarBuilder then a[#a+1] = 	{registered = false, name =L"WarBuilder", 				onLButton = warbuilderCommand} end
	if Phantom then a[#a+1] = 		{registered = false, name =L"Phantom", 					onLButton = Phantom.Show} end
	if CDown then a[#a+1] = 		{registered = false, name =L"CDown", 					onLButton = CDownSettings.Show} end
	if TacticSetNames then a[#a+1] ={registered = false, name =L"TacticSetNames", 			onLButton = TacticSetNames.SettingsWindow.OpenSettingsWindow} end
	if Vectors then a[#a+1] =		{registered = false, name =L"Vectors", 					onLButton = Vectors.Settings.Open} end
	
	-------------------------------
	--- cascading items
	-------------------------------
	if MiracleGrow2 then a[#a+1] = {
		registered = false, name =L"MGRemix <icon20470>", autosort = false,
		subitems = {
			[1] = {name = "Toggle", onLButton = MiracleGrow2.toggle},
			[2] = {name = "Config", onLButton = function () IraConfig.Open(MiracleGrow2.nConfigTab) end},
		}
	} end

	return a
end

function AddonButtonTogglers.registerButtons(buttons) 
	for k, v in ipairs(buttons) do
			-- TextLogAddEntry("Chat", 0, L"AddonButtonTogglers adding "..v.name)
			if (not v.registered) then
				-- add simple item
				if v.onLButton ~= nil then
					local button = LibAddonButton.Register("fx")
					LibAddonButton.AddMenuItem("fx", v.name, v.onLButton)
					v.registered = true

				-- add cascading item
				elseif v.subitems ~= nil then
					LibAddonButton.Register("fx")
					local menu = LibAddonButton.AddCascadingMenuItem("fx", v.name, v.autosort)
					for _, subitem in ipairs(v.subitems) do -- doesnt support multilevel subitems
						LibAddonButton.AddMenuSubitem(menu, subitem.name, subitem.onLButton)
					end
				end
			end
		end
end

function AddonButtonTogglers.HandleSlashCmd(input)
	if input == "" then
		TextLogAddEntry("Chat", 0, L"AddonButtonTogglers status:")
	end
end


function AddonButtonTogglers.OnShutdown()
	-- UnregisterEventHandler(SystemData.Events.CHAT_TEXT_ARRIVED, "whatsPugSc.OnChat")
end